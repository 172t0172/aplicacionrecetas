package com.example.myapplistas

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var recetas : ArrayList<String> = ArrayList()
        recetas.add("Mole")
        recetas.add("Tortas de carne polaca")
        recetas.add("Chiles rellenos")
        recetas.add("Enchiladas")

        var lista = findViewById<ListView>(R.id.lista)
        val adaptador = ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,recetas)
        lista.adapter = adaptador

        lista.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->

            when(recetas.get(position)){
                "Mole" -> {
                    val intent = Intent(this,MoleActivity::class.java)
                    startActivity(intent)
                }
            }
            when(recetas.get(position)){
                "Tortas de carne polaca" -> {
                    val intent = Intent(this,TortasActivity::class.java)
                    startActivity(intent)
                }
            }
            when(recetas.get(position)){
                "Chiles rellenos" -> {
                    val intent = Intent(this,ChilesActivity::class.java)
                    startActivity(intent)
                }
            }
            when(recetas.get(position)){
                "Enchiladas" -> {
                    val intent = Intent(this,EnchiladasActivity::class.java)
                    startActivity(intent)
                }
            }

            Toast.makeText(this,recetas.get(position), Toast.LENGTH_LONG).show()

        }
    }
}